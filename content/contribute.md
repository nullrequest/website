---
title: "A Dream to Develop On"
date: 2021-06-21T19:05:42+10:00
draft: false
Callout: "It's never been easier to contribute to your favorite distribution"
---

Impediments to contributing can easily be reduced and often eliminated at all. Serpent OS wants to reduce these frictions
and improve the efficiency of contributors/developers. The contributions of the community are pivotal to the success of
many open source projects. With Serpent OS, the line between being a user and a contributor has never been finer!

Some of the ways Serpent OS achieves this are by:

 - Using the well known GitLab workflow for packaging repositories
 - Maximising the performance of the clang compiler to minimise build times
   - LTO and PGO built `clang`
   - Static `LLVM` in `clang` binary
   - Using `LLVM's` binutils variants (such as the more performant `lld` linker)
 - `moss` store creates custom build roots in a jiffy so builds start compiling sooner
   - You only need extract a package once and will be available to use for all future builds
 - Utilise pre-compiled headers and unity builds where it reduces build times
   - `clang` enables you to instantiate templates of pre-compiled headers for extra benefit
 - Build files are simple but powerful
   - Easy to understand the build at a glance
   - Able to quickly add extra compiler tunables to build
   - Plug in a workload to enable profile guided optimizations in your build. You don't even need to know how it works!
 - Tooling takes care of steps that can be easily automated
   - Will continue to learn new tricks to suit new workflows
 - Lets you know when you've likely made an error in packaging so you can fix it up before someone else sees
   - Reduce the need to rework commits
